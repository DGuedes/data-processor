# InterSCity Data Processing Module

## Container Setup

1. Install docker and docker-compose

2. Clone this repository

3. Setup the submodules:
```
$ git submodule init
$ git submodule update
```

4. Add your docker host ip to `ADVERTISED_HOST` variable in `docker-compose.yml`.
You can get your ip using `ip addr show docker0`

## Shock setup

1. Run containers
```
$ docker-compose up -d
```

2. Check the log with
```
$ docker logs -f dataprocessor_master_1
```

## Running Shock (from container)

```
docker exec -it dataprocessor_master_1 ./bin/spark-submit --packages org.apache.spark:spark-streaming-kafka-0-8_2.11:2.1.0 --packages org.apache.spark:spark-sql-kafka-0-10_2.11:2.1.0 /shock/shock.py
```

## Kafka's interaction

1. Enter in Kafka's container:
```
$ docker exec -it kafka /bin/bash
```

2. Go to Kafka's home:
```
$ cd $KAFKA_HOME
```

3. Run scripts:
```
$ ./bin/kafka-topics.sh --list --zookeeper localhost:2181'
$ ./bin/kafka-console-producer.sh --topic new_pipeline_instruction --broker-list localhost:9092
newstream;{"file": "processing","ingest": "kafkasubscribe","topic": "interscity","brokers": "kafka:9092","name":"mystream"}
```

## Running Tests

1. Install pytest in the container
```
$ docker exec -it dataprocessor_master_1 pip install pytest
```

2. Run the tests
```
$ docker exec -it dataprocessor_master_1 pytest /shock
```


## Troubleshoot

First, be sure to check Shock's documentation (README file inside shock folder).

> Why it timeout spark packages (such as kafka-streaming, kafka-streaming-sql)?

Your container is not resolving names, probably. Append Google DNS or something
similar to your resolve.conf:
```
cat "nameserver 8.8.8.8" >> /etc/resolve.conf
```
